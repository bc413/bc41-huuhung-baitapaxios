const mockApi_Url = "https://63bea7f9f5cfc0949b5d489d.mockapi.io"

function fetNDList(){
    batLoading();
    axios({
        url: `${mockApi_Url}/quanLyNguoiDung`,
        method: "GET",
    })
    .then(function(res){
        tatLoading();
        renderND(res.data)
        return res.data
    })
    .catch(function(err){
        tatLoading();
        console.log(err)
    });
}
fetNDList()


function renderND(NDArr){
    let contentHTML = "";
    NDArr.forEach(function(item){
        let contentTr = `
            <tr>
                <td>${item.maND}</td>
                <td>${item.taiKhoan}</td>
                <td>${item.hoTen}</td>
                <td>${item.email}</td>
                <td>${item.loaiNguoiDung}</td>
                <td>${item.LoaiNgonNgu}</td>
                <td>${item.moTa}</td>
                <td>${item.hinhAnh}</td>
                <td class="d-flex">    
                    <button onclick="xoaND(${item.maND})" class="btn btn-danger mr-3">Xóa</button>
                    <button onclick="suaND(${item.maND})" class="btn btn-warning">Sửa</button>
                </td>
            </tr>
        `
        contentHTML = contentHTML+ contentTr;
    })
    document.querySelector("#tbodyFood").innerHTML = contentHTML
}

function xoaND(id){
    console.log(id)
    batLoading();
    axios({
        url: `${mockApi_Url}/quanLyNguoiDung/${id}`,
        method: "DELETE"
    })
    .then(function(res){
        tatLoading();
        fetNDList()
        console.log(res)
    })
    .catch(function(err){
        tatLoading();
        console.log(err)
    });
}

function thenND(){
    batLoading();
    let arrND = layThongTinTuForm();
    let matKhau = arrND.matKhau;
    let moTa = arrND.moTa;
    let kq1 = kiemTraNhap("#taiKhoan","#spanTaiKhoan",0);
    console.log(kiemTraTrung())
    let kq2 = kiemTraNhap("#hoTen","#spanHoTen",1) && checkHoTen();
    let kq3 = kiemTraNhap("#matKhau", "#spanMatKhau", 2) && checkMK() && checkDoDdai(matKhau, 6, 8, "#spanMatKhau");
    let kq4 = kiemTraNhap("#email", "#spanEmail",3) && checkEmail();
    let kq5 = kiemTraNhap("#loaiND", "#spanLoaiND",4);
    let kq6 = kiemTraNhap("#ngonNgu", "#spanNgonNgu",5);
    let kq7 = kiemTraNhap("#hinhAnh", "#spanHinhAnh",6)
    let kq8 = kiemTraNhap("#moTa", "#spanMoTa",7) && checkDoDdai(moTa, 0, 60, "#spanMoTa");
    if(kq1 && kq2 && kq3 && kq4 && kq5 && kq6 && kq7 && kq8){
        axios({
            url: `${mockApi_Url}/quanLyNguoiDung/`,
            method: "POST",
            data: layThongTinTuForm()
        })
        .then(function(res){
            tatLoading();
            fetNDList()
        })
        .catch(function(err){
            tatLoading();
            console.log(err)
        })
    }else{
        tatLoading();
    }
};

function suaND(id){
    document.getElementById("maND").disabled = true;
    batLoading();
    axios({
        url: `${mockApi_Url}/quanLyNguoiDung/${id}`,
        method: 'GET'
    })
    .then(function(res){
        tatLoading();
        showthongtin(res.data)
    })
    .catch(function(err){
        tatLoading();
        console.log(err)
    })
}


function capNhapND(){
    document.getElementById("maND").disabled = true;
    let data = layThongTinTuForm();
    console.log(data)
    batLoading();
    let arrND = layThongTinTuForm();
    let matKhau = arrND.matKhau;
    let moTa = arrND.moTa;
    let kq2 = kiemTraNhap("#hoTen","#spanHoTen",1) && checkHoTen();
    let kq3 = kiemTraNhap("#matKhau", "#spanMatKhau", 2) && checkMK() && checkDoDdai(matKhau, 6, 8, "#spanMatKhau");
    let kq4 = kiemTraNhap("#email", "#spanEmail",3) && checkEmail();
    let kq5 = kiemTraNhap("#loaiND", "#spanLoaiND",4);
    let kq6 = kiemTraNhap("#ngonNgu", "#spanNgonNgu",5);
    let kq7 = kiemTraNhap("#hinhAnh", "#spanHinhAnh",6)
    let kq8 = kiemTraNhap("#moTa", "#spanMoTa",7) && checkDoDdai(moTa, 0, 60, "#spanMoTa");
    if(kq2 && kq3 && kq4 && kq5 && kq6 && kq7 && kq8){
        axios({
            url: `${mockApi_Url}/quanLyNguoiDung/${data.maND}`,
            method: "PUT",
            data: data,
        })
        .then(function(res){
            tatLoading();
            fetNDList()
            console.log(res)
        })
        .catch(function(err){
            tatLoading();
            console.log(err)
        })
    }else{
        tatLoading();
    }
}