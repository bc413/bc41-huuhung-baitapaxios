let thongBao = ["Hãy nhập tài khoản","Hãy nhập họ tên","Hãy nhập mật khẩu","Hãy nhập Email","Hãy chọn loại người dùng", "Hãy chọn ngôn ngữ", "Hãy nhập hình ảnh","Hãy nhập mô tả"];


function kiemTraNhap(domId,spanResua,index){
    let valueInput = document.querySelector(domId).value;
    if(valueInput == ""){
        document.querySelector(spanResua).style.display="block"
        document.querySelector(spanResua).innerHTML = thongBao[index]
        return false
    }else{
        document.querySelector(spanResua).style.display="none"
        return true;
    }
}

function kiemTraTrung(){
    let arrND = layThongTinTuForm();
    let taiKhoan = arrND.taiKhoan
    axios({
        url: "https://63bea7f9f5cfc0949b5d489d.mockapi.io/quanLyNguoiDung",
        method: "GET",
    })
    .then(function(res){
        let data = res.data
        console.log(data)
        let viTri = data.findIndex(function(item){
            return  item.taiKhoan == taiKhoan;
        })
        if(viTri !== -1){
            document.querySelector("#spanTaiKhoan").style.display="block"
            document.querySelector("#spanTaiKhoan").innerHTML = `<span>Tài khoản bị trùng</span>`
            return false;
        }else{
            document.querySelector("#spanTaiKhoan").style.display="none"
            return true;
        }
    })
    .catch(function(err){
        console.log(err)
    });
}

console.log()

function checkHoTen(){
    let arrND = layThongTinTuForm();
    let hoTen = arrND.hoTen
    console.log(hoTen)
    let kyTu = new RegExp("^[A-Za-z]+$");
    if(kyTu.test(hoTen)){
        document.querySelector("#spanHoTen").style.display="none"
        return true;
    }else{
        document.querySelector("#spanHoTen").style.display="block"
        document.querySelector("#spanHoTen").innerHTML = `<span>Họ tên không chứa số và ký tự đặc biệt</span>`
        return false;
    }
};

function checkMK(){
    let arrND = layThongTinTuForm();
    let hoTen = arrND.matKhau
    let kyTu = new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])");
    if(kyTu.test(hoTen)){
        document.querySelector("#spanMatKhau").style.display="none"
        return true;
    }else{
        document.querySelector("#spanMatKhau").innerHTML = "Mật khẩu phải có ít nhất 1 số, 1 chữ in hoa, 1 ký tự đặc biệt "
        document.querySelector("#spanMatKhau").style.display="block";
        return false;
    }
}

function checkEmail(){
    let arrND = layThongTinTuForm();
    let email = arrND.email
    console.log(email)
    let kyTu = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(email.match(kyTu)){
        document.querySelector("#spanEmail").style.display = "none";
        return true;
    }else{
        document.querySelector("#spanEmail").style.display = "Block";
        document.querySelector("#spanEmail").innerHTML = "Vui lòng đúng email"
        return false;
    }
}

function checkDoDdai(value, min, max, showSpan){
    if(value.length < min || value.length > max ){
        document.querySelector(showSpan).style.display = "Block";
        document.querySelector(showSpan).innerHTML = `Nội dung phải từ ${min} đến ${max} ký tự`
        return false;
    }else{
        document.querySelector(showSpan).style.display = "none";
        return true;
    }
}